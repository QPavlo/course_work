package com.Errors;

import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public class ErrorAnimation {
    private TranslateTransition transition;

    public ErrorAnimation(Node node) {
        this.transition = new TranslateTransition(Duration.millis(70), node);
        this.transition.setFromX(-10f);
        this.transition.setByX(10f);
        this.transition.setCycleCount(3);
        this.transition.setAutoReverse(true);
    }

    public void PlayErrorAnimation() {
        this.transition.playFromStart();
    }
}
