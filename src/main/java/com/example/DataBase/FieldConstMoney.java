package com.example.DataBase;

public class FieldConstMoney {
    public static final String CUSTOMER_PROGRESS_TABLE = "customerprogress";
    public static final String CUSTOMER_PROGRESS_MONTH = "curr_month";
    public static final String CUSTOMER_PROGRESS_MONEY = "money";
}
