package com.example.DataBase;

import com.example.GUI.MainApplication;
import com.functional.Customer;
import com.functional.CustomerProgress;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

public class CustomerDataBaseHandler {
    private Config config;
    private Connection dbConnection;

    public CustomerDataBaseHandler() {
        this.config = new Config();
    }

    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connection = "jdbc:mysql://" + this.config.dbHost + ":" + this.config.dbPort + "/" + this.config.dbName;

        Class.forName("com.mysql.jdbc.Driver");

        this.dbConnection = DriverManager.getConnection(connection, this.config.dbUser, this.config.dbPass);
        return dbConnection;

    }

    public boolean signUpUser(Customer customer) {
        String insert = "INSERT INTO " + FieldConstCustomers.CUSTOMER_TABLE + " (" +
                FieldConstCustomers.CUSTOMER_FIRST_NAME + "," +
                FieldConstCustomers.CUSTOMER_LAST_NAME + "," +
                FieldConstCustomers.CUSTOMER_USER_NAME + "," +
                FieldConstCustomers.CUSTOMER_PASSWORD + ")" +
                "VALUES(?,?,?,?)";
        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(insert);
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getUserName());
            preparedStatement.setString(4, customer.getPassword());
            preparedStatement.executeUpdate();

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ResultSet getCustomer(Customer customer) {
        ResultSet resultSet = null;
        String select = "SELECT * FROM " + FieldConstCustomers.CUSTOMER_TABLE +
                " WHERE " + FieldConstCustomers.CUSTOMER_USER_NAME +
                "=? AND " + FieldConstCustomers.CUSTOMER_PASSWORD +
                "=?";
        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(select);
            preparedStatement.setString(1, customer.getUserName());
            preparedStatement.setString(2, customer.getPassword());
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public boolean addOfferToCustomer(Customer customer) {
        String update = "UPDATE " +
                FieldConstCustomers.CUSTOMER_TABLE +
                " SET " + FieldConstCustomers.CUSTOMER_DEPOSIT_ID + " =?, " +
                FieldConstCustomers.CUSTOMER_MAIN_BALANCE + " =?, " +
                FieldConstCustomers.CUSTOMER_MONTHS + " =? " +
                " WHERE " + FieldConstCustomers.CUSTOMER_USER_NAME +
                " =? ";
        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(update);
            preparedStatement.setInt(1, customer.getDepositID());
            preparedStatement.setDouble(2, customer.getMainBalance());
            preparedStatement.setInt(3, customer.getMonths());
            preparedStatement.setString(4, customer.getUserName());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Customer getRegisteredCustomer(String userName) {
        Customer tempCustomer = new Customer();

        String select = "SELECT * FROM " + FieldConstCustomers.CUSTOMER_TABLE +
                " WHERE " + FieldConstCustomers.CUSTOMER_USER_NAME +
                "=? ";
        try {

            PreparedStatement preparedStatement = getDbConnection().prepareStatement(select);
            preparedStatement.setString(1, userName);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                tempCustomer.setCustomerID(resultSet.getInt(FieldConstCustomers.CUSTOMER_ID));
                tempCustomer.setFirstName(resultSet.getString(FieldConstCustomers.CUSTOMER_FIRST_NAME));
                tempCustomer.setLastName(resultSet.getString(FieldConstCustomers.CUSTOMER_LAST_NAME));
                tempCustomer.setUserName(resultSet.getString(FieldConstCustomers.CUSTOMER_USER_NAME));
                tempCustomer.setDepositBalance(resultSet.getDouble(FieldConstCustomers.CUSTOMER_DEPOSIT_BALANCE));
                tempCustomer.setMainBalance(resultSet.getDouble(FieldConstCustomers.CUSTOMER_MAIN_BALANCE));
                tempCustomer.setCurrentMonth(resultSet.getInt(FieldConstCustomers.CUSTOMER_CURRENT_MONTH));
                tempCustomer.setMonths(resultSet.getInt(FieldConstCustomers.CUSTOMER_MONTHS));
                tempCustomer.setDepositID(resultSet.getInt(FieldConstCustomers.CUSTOMER_DEPOSIT_ID));
                tempCustomer.setPassword(resultSet.getString(FieldConstCustomers.CUSTOMER_PASSWORD));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return tempCustomer;
    }

    public boolean updateCustomerInformation(Customer customer) {
        String update = "UPDATE " +
                FieldConstCustomers.CUSTOMER_TABLE +
                " SET " + FieldConstCustomers.CUSTOMER_DEPOSIT_BALANCE + " =?, " +
                FieldConstCustomers.CUSTOMER_MAIN_BALANCE + " =?, " +
                FieldConstCustomers.CUSTOMER_CURRENT_MONTH + " =? " +
                " WHERE " + FieldConstCustomers.CUSTOMER_USER_NAME +
                "=? ";
        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(update);
            preparedStatement.setDouble(1, customer.getDepositBalance());
            preparedStatement.setDouble(2, customer.getMainBalance());
            preparedStatement.setInt(3, customer.getCurrentMonth());
            preparedStatement.setString(4, customer.getUserName());//MainApplication.customerUserName);
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean createCustomerTable(Customer customer) {

        String create = "CREATE TABLE IF NOT EXISTS " + FieldConstMoney.CUSTOMER_PROGRESS_TABLE +
                customer.getCustomerID() +
                " (" +
                " curr_month INT," +
                " money DOUBLE" +
                " ) ";

        try {
            Statement statement = getDbConnection().createStatement();
            statement.executeUpdate(create);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean addInfoAboutCustomerMoney(Customer customer) {
        String insert = "INSERT INTO " + FieldConstMoney.CUSTOMER_PROGRESS_TABLE + customer.getCustomerID() +
                " (" +
                FieldConstMoney.CUSTOMER_PROGRESS_MONTH + "," +
                FieldConstMoney.CUSTOMER_PROGRESS_MONEY +
                ")" +
                "VALUES(?,?)";
        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(insert);
            preparedStatement.setInt(1, customer.getCurrentMonth());
            preparedStatement.setDouble(2, customer.getDepositBalance());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ObservableList<CustomerProgress> fillArraysByMoneyAndMonths() {
        ObservableList<CustomerProgress> customerProgresses = FXCollections.observableArrayList();
        String select = "SELECT * FROM " + FieldConstMoney.CUSTOMER_PROGRESS_TABLE +
                MainApplication.registeredCustomer.getCustomerID();
        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(select);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerProgresses.add(new CustomerProgress(
                        resultSet.getInt("curr_month"),
                        resultSet.getDouble("money"))
                );
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return customerProgresses;
    }

    public void dropCustomerTable(Customer customer) {
        String drop = "DROP TABLE IF EXISTS " + FieldConstMoney.CUSTOMER_PROGRESS_TABLE
                + customer.getCustomerID();
        try {
            Statement statement = getDbConnection().prepareStatement(drop);
            statement.executeUpdate(drop);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

}
