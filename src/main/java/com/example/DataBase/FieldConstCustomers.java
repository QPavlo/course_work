package com.example.DataBase;

public class FieldConstCustomers {
    public static final String CUSTOMER_TABLE = "customers";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String CUSTOMER_FIRST_NAME = "first_name";
    public static final String CUSTOMER_LAST_NAME = "last_name";
    public static final String CUSTOMER_USER_NAME = "user_name";
    public static final String CUSTOMER_DEPOSIT_BALANCE = "deposit_balance";
    public static final String CUSTOMER_MAIN_BALANCE = "main_balance";
    public static final String CUSTOMER_CURRENT_MONTH = "current_month";
    public static final String CUSTOMER_MONTHS = "months";
    public static final String CUSTOMER_DEPOSIT_ID = "deposit_id";
    public static final String CUSTOMER_PASSWORD = "password";
}
