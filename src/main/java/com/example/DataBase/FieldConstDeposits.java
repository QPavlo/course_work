package com.example.DataBase;

public class FieldConstDeposits {
    public static final String DEPOSIT_TABLE = "deposits";
    public static final String DEPOSIT_BANK = "bank_name";
    public static final String DEPOSIT_MONTH = "months";
    public static final String DEPOSIT_PERCENT = "percent";
}
