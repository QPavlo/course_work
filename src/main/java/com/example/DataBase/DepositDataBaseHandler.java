package com.example.DataBase;

import com.example.GUI.AccountController;
import com.functional.Deposit;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.sql.*;
import java.util.List;

import static com.example.GUI.AccountController.floatRegex;
import static com.example.GUI.AccountController.regex;

public class DepositDataBaseHandler {
    private final Config config;

    public DepositDataBaseHandler() {
        this.config = new Config();
    }

    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connection = "jdbc:mysql://" + this.config.dbHost + ":" + this.config.dbPort + "/" + this.config.dbName;

        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(connection, this.config.dbUser, this.config.dbPass);
    }

    public ObservableList<Deposit> getDepositInfo() {
        ObservableList<Deposit> depositsInfo = FXCollections.observableArrayList();
        String select = "SELECT * FROM " + FieldConstDeposits.DEPOSIT_TABLE;
        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(select);
            ResultSet resultSet = preparedStatement.executeQuery();
            for (int i = 0; resultSet.next(); ++i) {
                depositsInfo.add(new Deposit(i,
                        resultSet.getString("bank_name"),
                        Integer.parseInt(resultSet.getString("months")),
                        Double.parseDouble(resultSet.getString("percent"))));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return depositsInfo;
    }

    public ObservableList<Deposit> getDepositByCheckBox(List<CheckBox> bankNames, List<CheckBox> months,
                                                        TextField percent1, TextField percent2, Text mistakeInPercents) {
        ObservableList<Deposit> depositsInfo = FXCollections.observableArrayList();

        StringBuilder select = new StringBuilder("SELECT * FROM " + FieldConstDeposits.DEPOSIT_TABLE
                + " WHERE FALSE ");

        for (CheckBox bankName : bankNames) {
            if (bankName.isSelected()) {
                select.append(" OR " + FieldConstDeposits.DEPOSIT_BANK + " = " + " '").append(bankName.getText()).append("' ");
                select.append(getByMonths(months));
                select.append(getByPercents(percent1, percent2));
            }
        }

        for (CheckBox month : months) {
            if (month.isSelected()) {
                select.append(" OR " + FieldConstDeposits.DEPOSIT_MONTH + " = " + " '").append(month.getText()).append("' ");
                select.append(getByBankNames(bankNames));
                select.append(getByPercents(percent1, percent2));
            }
        }

        if (percent1.getText().matches(floatRegex) && percent2.getText().matches(floatRegex)) {
            select.append(" OR ( " + FieldConstDeposits.DEPOSIT_PERCENT + " >= " + " '").append(percent1.getText()).append("' ");
            select.append(" AND " + FieldConstDeposits.DEPOSIT_PERCENT + " <= " + " '").append(percent2.getText()).append("' )");
            select.append(getByMonths(months));
            select.append(getByBankNames(bankNames));
        }
        else {
           mistakeInPercents.setText("Input numbers only");
        }

        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(select.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            for (int i = 0; resultSet.next(); ++i) {
                depositsInfo.add(new Deposit(i,
                        resultSet.getString("bank_name"),
                        Integer.parseInt(resultSet.getString("months")),
                        Double.parseDouble(resultSet.getString("percent"))));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return depositsInfo;

    }

    private String getByMonths(List<CheckBox> months) {
        StringBuilder select = new StringBuilder();
        boolean flag = false;
        for (CheckBox month : months) {
            if (month.isSelected()) {
                select.append(" AND ( FALSE ");
                flag = true;
                break;
            }
        }
        if (flag) {
            for (CheckBox month : months) {
                if (month.isSelected()) {
                    select.append(" OR " + FieldConstDeposits.DEPOSIT_MONTH + " = " + " '").append(month.getText()).append("' ");
                }
            }
            select.append(" )");
        }
        return select.toString();
    }

    private String getByBankNames(List<CheckBox> bankNames) {
        StringBuilder select = new StringBuilder();
        boolean flag = false;
        for (CheckBox bankName : bankNames) {
            if (bankName.isSelected()) {
                select.append(" AND ( FALSE ");
                flag = true;
                break;
            }
        }
        if (flag) {
            for (CheckBox bankName : bankNames) {
                if (bankName.isSelected()) {
                    select.append(" OR " + FieldConstDeposits.DEPOSIT_MONTH + " = " + " '").append(bankName.getText()).append("' ");
                }
            }
            select.append(" )");
        }
        return select.toString();
    }

    private String getByPercents(TextField percent1, TextField percent2) {
        StringBuilder select = new StringBuilder();
        if (percent1.getText().matches(floatRegex) && percent1.getText().matches(floatRegex)) {
            select.append(" AND ( FALSE ");
            if (percent1.getText().trim().matches(floatRegex)) {
                select.append(" OR " + FieldConstDeposits.DEPOSIT_PERCENT + " >= " + " '").append(percent1.getText()).append("' ");
            }
            if (percent1.getText().trim().matches(floatRegex)) {
                select.append(" AND " + FieldConstDeposits.DEPOSIT_PERCENT + " <= " + " '").append(percent2.getText()).append("' ");
            }
            select.append(" )");
        }
        return select.toString();
    }
}


