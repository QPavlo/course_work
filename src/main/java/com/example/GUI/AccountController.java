package com.example.GUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.example.DataBase.CustomerDataBaseHandler;
import com.example.DataBase.DepositDataBaseHandler;
import com.functional.Deposit;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AccountController {

    final static public String regex = "\\d+";
    final static public String floatRegex = "^[-+]?([0-9]+(\\.[0-9]+)?|\\.[0-9]+)$";

    @FXML
    private TableView<Deposit> DepositTable;

    @FXML
    public Text mistakePercentInput;

    @FXML
    private Button defaultViewButton;

    @FXML
    private Button enterPercentButton;

    @FXML
    private CheckBox BankNameBox1;

    @FXML
    private CheckBox BankNameBox2;

    @FXML
    private CheckBox BankNameBox3;

    @FXML
    private Text initialReplenish;

    @FXML
    private TitledPane monthSelect;

    @FXML
    private CheckBox FirstMonthBox;

    @FXML
    private TableColumn<Deposit, Integer> MonthsCol;

    @FXML
    private TableColumn<Deposit, Double> PercentCol;

    @FXML
    private CheckBox SecondMonthBox;

    @FXML
    private Button SelectButtonAcc;

    @FXML
    private CheckBox ThirdMonthBox;

    @FXML
    private TextField InitialSumMoney;

    @FXML
    private Text greetingText;

    @FXML
    private TextField durationInMonthsField;

    @FXML
    private Text NoSelectText;

    @FXML
    private TableColumn<Deposit, String> bankNameCol;

    @FXML
    private TextField fromPercentField;

    @FXML
    private TextField toPercentField;

    private ObservableList<Deposit> depositsList;

    private CustomerDataBaseHandler customerDataBaseHandler = null;

    private DepositDataBaseHandler depositDataBaseHandler = null;

    private final List<CheckBox> monthsBoxes = new ArrayList<>(3);
    private final List<CheckBox> bankNames = new ArrayList<>(3);

    private void fillLists() {
        monthsBoxes.add(0, FirstMonthBox);
        monthsBoxes.add(1, SecondMonthBox);
        monthsBoxes.add(2, ThirdMonthBox);

        bankNames.add(0, BankNameBox1);
        bankNames.add(1, BankNameBox2);
        bankNames.add(2, BankNameBox3);
    }

    @FXML
    void initialize() {

        customerDataBaseHandler = new CustomerDataBaseHandler();

        initialReplenish.setText("- set the initial amount of money on your main account here:");
        depositDataBaseHandler = new DepositDataBaseHandler();
        bankNameCol.setCellValueFactory(new PropertyValueFactory<>("bankName"));
        MonthsCol.setCellValueFactory(new PropertyValueFactory<>("months"));
        PercentCol.setCellValueFactory(new PropertyValueFactory<>("percent"));

        greetingText.setText("Hello " + MainApplication.registeredCustomer.getFirstName() + ",");
        depositsList = depositDataBaseHandler.getDepositInfo();
        DepositTable.setItems(depositsList);

        BankNameBox1.setOnAction(event -> {
            fillLists();
            depositsList = depositDataBaseHandler.getDepositByCheckBox(bankNames, monthsBoxes,
                    fromPercentField, toPercentField, mistakePercentInput);
            DepositTable.setItems(depositsList);
        });

        BankNameBox2.setOnAction(event -> {
            fillLists();
            depositsList = depositDataBaseHandler.getDepositByCheckBox(bankNames, monthsBoxes,
                    fromPercentField, toPercentField, mistakePercentInput);
            DepositTable.setItems(depositsList);
        });

        BankNameBox3.setOnAction(event -> {
            fillLists();
            depositsList = depositDataBaseHandler.getDepositByCheckBox(bankNames, monthsBoxes,
                    fromPercentField, toPercentField, mistakePercentInput);
            DepositTable.setItems(depositsList);
        });

        FirstMonthBox.setOnAction(event -> {
            fillLists();
            depositsList = depositDataBaseHandler.getDepositByCheckBox(bankNames, monthsBoxes,
                    fromPercentField, toPercentField, mistakePercentInput);
            DepositTable.setItems(depositsList);
        });

        SecondMonthBox.setOnAction(event -> {

            fillLists();
            depositsList = depositDataBaseHandler.getDepositByCheckBox(bankNames, monthsBoxes,
                    fromPercentField, toPercentField, mistakePercentInput);
            DepositTable.setItems(depositsList);
        });

        ThirdMonthBox.setOnAction(event -> {
            fillLists();
            depositsList = depositDataBaseHandler.getDepositByCheckBox(bankNames, monthsBoxes,
                    fromPercentField, toPercentField, mistakePercentInput);
            DepositTable.setItems(depositsList);
        });

        enterPercentButton.setOnAction(event -> {
            fillLists();
            depositsList = depositDataBaseHandler.getDepositByCheckBox(bankNames, monthsBoxes,
                    fromPercentField, toPercentField, mistakePercentInput);
            DepositTable.setItems(depositsList);
        });

        defaultViewButton.setOnAction(event -> {
            depositsList = depositDataBaseHandler.getDepositInfo();
            mistakePercentInput.setText("");
            DepositTable.setItems(depositsList);
            BankNameBox1.setSelected(false);
            BankNameBox2.setSelected(false);
            BankNameBox3.setSelected(false);
            FirstMonthBox.setSelected(false);
            SecondMonthBox.setSelected(false);
            ThirdMonthBox.setSelected(false);
            fromPercentField.clear();
            toPercentField.clear();
        });

        if (MainApplication.registeredCustomer.getMainBalance() > 0) {
            initialReplenish.setText("- replenish your main account ( it is not necessary ) here:");
        }

        DepositTable.setOnMouseClicked(mouseEvent -> {
            Deposit depositTemp = DepositTable.getSelectionModel().getSelectedItem();
            try {
                SelectButtonAcc.setOnAction(event -> {
                    mistakePercentInput.setText("");
                    if (!InitialSumMoney.getText().matches(floatRegex) && MainApplication.registeredCustomer.getMainBalance() <= 0
                            || !durationInMonthsField.getText().matches(floatRegex)) {
                        NoSelectText.setText("you not selected and not set(or incorrect data format)");
                    } else {
                        if (MainApplication.registeredCustomer.getMainBalance() > 0
                                && InitialSumMoney.getText().trim().equals("")) {
                            InitialSumMoney.setText("0");
                        }
                        MainApplication.registeredCustomer.setDepositID(depositTemp.getDepositID());
                        MainApplication.registeredCustomer.replenishMainAccount(Double.parseDouble(InitialSumMoney.getText()));
                        MainApplication.registeredCustomer.setSelectedDeposit(depositTemp);

                        int tempResOfSet = MainApplication.registeredCustomer.setMonths(Integer.parseInt(durationInMonthsField.getText()));

                        Alert alert = new Alert(Alert.AlertType.WARNING);

                        alert.setTitle("Pay attention");

                        switch (tempResOfSet) {
                            case 1, 2 -> { // (1 round to lesser) ,(2 round to bigger)
                                alert.setContentText("You input not a multiple value(months), automatically round to : " +
                                        MainApplication.registeredCustomer.getMonths());
                                alert.showAndWait();
                            }
                            case 3 -> {
                                alert.setContentText("You input to small value(months), automatically round to : " +
                                        MainApplication.registeredCustomer.getMonths());
                                alert.showAndWait();
                            }
                        }

                        depositDataBaseHandler.getDepositInfo();
                        SelectButtonAcc.getScene().getWindow().hide();
                        FXMLLoader fxmlLoader = new FXMLLoader();
                        fxmlLoader.setLocation(getClass().getResource("ChosenDepAccView.fxml"));

                        try {
                            fxmlLoader.load();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Parent root = fxmlLoader.getRoot();
                        Stage stage = new Stage();
                        stage.setScene(new Scene(root));
                        stage.show();

                        if (!customerDataBaseHandler.addOfferToCustomer(MainApplication.registeredCustomer)) {
                            System.out.println("no good");
                        }
                    }
                });
            } catch (NullPointerException exception) {
                System.out.println(exception.getMessage());
            }
        });

        SelectButtonAcc.setOnAction(event -> {
            if (MainApplication.registeredCustomer.getDepositID() <= 0 && !InitialSumMoney.getText().trim().matches(floatRegex)) {
                NoSelectText.setText("you not selected and not set(or incorrect data format)");
            } else if (MainApplication.registeredCustomer.getDepositID() <= 0) {
                NoSelectText.setText("you not selected any options");
            } else if (!InitialSumMoney.getText().trim().matches(floatRegex) || !durationInMonthsField.getText().trim().matches(regex)) {
                NoSelectText.setText("you set incorrect data format");
            }
        });
    }
}
