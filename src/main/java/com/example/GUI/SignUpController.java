package com.example.GUI;

import java.io.IOException;

import com.Errors.ErrorAnimation;
import com.example.DataBase.CustomerDataBaseHandler;
import com.functional.Customer;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SignUpController {

    @FXML
    private Button SignUpButtonUp;

    @FXML
    private Text SignUpErrorUserName;

    @FXML
    private Button signInButtonSignUp;

    @FXML
    private TextField signUpFNameField;

    @FXML
    private TextField signUpLNameField;

    @FXML
    private PasswordField signUpPassField;

    @FXML
    private TextField signUpUNameField;

    private CustomerDataBaseHandler customerDataBaseHandler;

    @FXML
    void initialize() {
        SignUpButtonUp.setOnAction(event -> {
            customerDataBaseHandler = new CustomerDataBaseHandler();
            if (createNewUser()) {
                MainApplication.registeredCustomer = customerDataBaseHandler.getRegisteredCustomer(MainApplication.registeredCustomer.getUserName());
                SignUpButtonUp.getScene().getWindow().hide();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("AccountView.fxml"));

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Parent root = fxmlLoader.getRoot();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.show();
            } else {
                ErrorAnimation userNameMaybeNoUnique = new ErrorAnimation(signUpUNameField);
                SignUpErrorUserName.setText("Unable to add user (maybe the username is already taken)");
                userNameMaybeNoUnique.PlayErrorAnimation();
            }
        });

        signInButtonSignUp.setOnAction(event -> {
            signInButtonSignUp.getScene().getWindow().hide();
            FXMLLoader fxmlLoader1 = new FXMLLoader();
            fxmlLoader1.setLocation(getClass().getResource("SignInView.fxml"));
            try {
                fxmlLoader1.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = fxmlLoader1.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
        });

    }

    private boolean createNewUser() {
        CustomerDataBaseHandler customerDataBaseHandler = new CustomerDataBaseHandler();
        boolean successfulCreating = true;
        String firstName = signUpFNameField.getText().trim();
        String lastName = signUpLNameField.getText().trim();
        String userName = signUpUNameField.getText().trim();
        String password = signUpPassField.getText().trim();
        if (firstName.equals("")) {
            ErrorAnimation firstNameAnimation = new ErrorAnimation(signUpFNameField);
            firstNameAnimation.PlayErrorAnimation();
            successfulCreating = false;
        }
        if (lastName.equals("")) {
            ErrorAnimation lastNameAnimation = new ErrorAnimation(signUpLNameField);
            lastNameAnimation.PlayErrorAnimation();
            successfulCreating = false;
        }
        if (userName.equals("")) {
            ErrorAnimation userNameAnimation = new ErrorAnimation(signUpUNameField);
            userNameAnimation.PlayErrorAnimation();
            successfulCreating = false;
        }
        if (password.equals("")) {
            ErrorAnimation passAnimation = new ErrorAnimation(signUpPassField);
            passAnimation.PlayErrorAnimation();
            successfulCreating = false;
        }

        if (!successfulCreating) {
            return false;
        }

        Customer tempCustomer = new Customer(firstName, lastName, userName, password);
        MainApplication.registeredCustomer = tempCustomer;
        return customerDataBaseHandler.signUpUser(tempCustomer);
    }

}
