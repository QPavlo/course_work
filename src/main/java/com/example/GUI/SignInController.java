package com.example.GUI;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.Errors.ErrorAnimation;
import com.example.DataBase.CustomerDataBaseHandler;
import com.example.DataBase.DepositDataBaseHandler;
import com.functional.Customer;
import com.functional.Deposit;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.text.Text;

public class SignInController {

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button signInButton;

    @FXML
    private Text ErrorSignInText;

    @FXML
    private Button signUpButton;

    @FXML
    private TextField userNameField;

    @FXML
    public void initialize() {
        signInButton.setOnAction(event -> {
            String userNameText = userNameField.getText().trim();
            String passwordText = passwordField.getText().trim();
            if (!userNameText.equals("") && !passwordText.equals("")) {
                if (SignInUser(userNameText, passwordText)) {
                    signInButton.getScene().getWindow().hide();
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    if (MainApplication.registeredCustomer.getDepositID() <= 0) {
                        fxmlLoader.setLocation(getClass().getResource("AccountView.fxml"));
                    } else {
                        fxmlLoader.setLocation(getClass().getResource("ChosenDepAccView.fxml"));
                    }
                    try {
                        fxmlLoader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Parent root = fxmlLoader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));
                    stage.show();
                }
            } else {
                ErrorSignInText.setText("Empty field(s) !");
            }
        });

        signUpButton.setOnAction(event -> {
            signUpButton.getScene().getWindow().hide();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("SignUpView.fxml"));
            try {
                fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Parent root = fxmlLoader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        });
    }

    private boolean SignInUser(String userNameText, String passwordText) {
        CustomerDataBaseHandler customerDataBaseHandler = new CustomerDataBaseHandler();
        DepositDataBaseHandler depositDataBaseHandler = new DepositDataBaseHandler();
        Customer customer = new Customer();
        customer.setUserName(userNameText);
        customer.setPassword(passwordText);
        ResultSet resultSet = customerDataBaseHandler.getCustomer(customer);
        try {
            if (!resultSet.next()) {
                ErrorAnimation userNameAnimation = new ErrorAnimation(this.userNameField);
                ErrorAnimation passwordAnimation = new ErrorAnimation(this.passwordField);
                userNameAnimation.PlayErrorAnimation();
                passwordAnimation.PlayErrorAnimation();
                ErrorSignInText.setText("Incorrect user name or password!");
                userNameField.clear();
                passwordField.clear();
                return false;
            } else {
                ObservableList<Deposit> depositsList = depositDataBaseHandler.getDepositInfo();
                MainApplication.registeredCustomer = customerDataBaseHandler.getRegisteredCustomer(customer.getUserName());
                if (MainApplication.registeredCustomer.getDepositID() > 0) {
                    MainApplication.registeredCustomer.setSelectedDeposit(depositsList.get(MainApplication.registeredCustomer.getDepositID()));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
