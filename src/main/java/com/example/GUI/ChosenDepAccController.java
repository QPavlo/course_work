package com.example.GUI;

import com.example.DataBase.CustomerDataBaseHandler;
import com.functional.CustomerProgress;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class ChosenDepAccController {

    @FXML
    private LineChart<?, ?> DepositChart;

    @FXML
    private CategoryAxis MonthsAxis;

    @FXML
    private Text bankNameText;

    @FXML
    private Text currMonthText;

    @FXML
    private Text depositMoneyText;

    @FXML
    private Text greetingText;

    @FXML
    private Text mainAccMoney;

    @FXML
    private NumberAxis moneyAxis;

    @FXML
    private Text monthsText;

    @FXML
    private Text percentText;

    @FXML
    private Button replenishButton;

    @FXML
    private Button replenishButtonMain;

    @FXML
    private TextField replenishField;

    @FXML
    private TextField replenishFieldMain;

    @FXML
    private Text failReplenishment;

    @FXML
    private Button startDepositButton;

    @FXML
    private CheckBox confirmCheckbox;

    @FXML
    private Button endOfDepositButton;

    @FXML
    private Text failReplenishmentMain;

    @FXML
    private Text notConfirmText;

    @FXML
    private Button skipOneMonth;

    @FXML
    private Text totalMonthText;

    @FXML
    private Text EndOfDeposit;


    @FXML
    private Button withdrawalButton;

    void drawChart(ObservableList<CustomerProgress> progress) {
        XYChart.Series series = new XYChart.Series<>();
        DepositChart.getData().clear();
        for (CustomerProgress customerProgress : progress) {
            series.getData().add(new XYChart.Data<>(Integer.toString(customerProgress.getMonth()), customerProgress.getMoney()));
        }
        DepositChart.getData().addAll(series);
    }

    @FXML
    void initialize() {
        endOfDepositButton.setVisible(false);
        startDepositButton.setVisible(false);
        if (MainApplication.registeredCustomer.getCurrentMonth() < 0) {
            skipOneMonth.setVisible(false);
        }

        CustomerDataBaseHandler customerDataBaseHandler = new CustomerDataBaseHandler();
        ObservableList<CustomerProgress> customerProgresses = FXCollections.observableArrayList();
        if (MainApplication.registeredCustomer.getCurrentMonth() >= 0) {
            customerProgresses = customerDataBaseHandler.fillArraysByMoneyAndMonths();
        }
        if (customerProgresses.size() >= 1) {
            drawChart(customerProgresses);
        }

        if (MainApplication.registeredCustomer.getDepositBalance() <= 0) {
            EndOfDeposit.setFill(Color.GOLD);
            EndOfDeposit.setText("You have not money on deposit account");
        }
        notConfirmText.setText("");
        failReplenishment.setText("");

        mainAccMoney.setText(Double.toString(MainApplication.registeredCustomer.getMainBalance()));
        depositMoneyText.setText(Double.toString(MainApplication.registeredCustomer.getDepositBalance()));
        currMonthText.setText(Integer.toString(MainApplication.registeredCustomer.getCurrentMonth()));

        totalMonthText.setText(Integer.toString(MainApplication.registeredCustomer.getMonths()));
        bankNameText.setText(MainApplication.registeredCustomer.getSelectedDeposit().getBankName());
        monthsText.setText(Integer.toString(MainApplication.registeredCustomer.getSelectedDeposit().getMonths()));
        percentText.setText(Double.toString(MainApplication.registeredCustomer.getSelectedDeposit().getPercent()));

        replenishButton.setOnAction(event -> {
            if (!replenishField.getText().trim().matches(AccountController.floatRegex)) {
                failReplenishment.setText("incorrect data format");
                replenishField.clear();
                return;
            }
            failReplenishment.setText("");
            failReplenishmentMain.setText("");
            notConfirmText.setText("");
            if (MainApplication.registeredCustomer.getMainBalance() >= Double.parseDouble(replenishField.getText())) {
                MainApplication.registeredCustomer.replenishDepositAccount(Double.parseDouble(replenishField.getText()));
                MainApplication.registeredCustomer.withdrawFundsFromMainAccount(Double.parseDouble(replenishField.getText()));
                depositMoneyText.setText(Double.toString(MainApplication.registeredCustomer.getDepositBalance()));
                mainAccMoney.setText(Double.toString(MainApplication.registeredCustomer.getMainBalance()));
                EndOfDeposit.setText("");
                replenishField.clear();
                customerDataBaseHandler.updateCustomerInformation(MainApplication.registeredCustomer);
                if (MainApplication.registeredCustomer.getCurrentMonth() >= 0) {
                    customerDataBaseHandler.addInfoAboutCustomerMoney(MainApplication.registeredCustomer);
                }
                if (MainApplication.registeredCustomer.getCurrentMonth() <= -1 && MainApplication.registeredCustomer.getDepositBalance() > 0) {
                    startDepositButton.setVisible(true);
                }
                if (MainApplication.registeredCustomer.getCurrentMonth() > 0) {
                    drawChart(customerDataBaseHandler.fillArraysByMoneyAndMonths());
                }
            } else {
                failReplenishment.setText("You have not enough money");
                replenishField.clear();
            }
        });


        if (MainApplication.registeredCustomer.getCurrentMonth() <= -1 && MainApplication.registeredCustomer.getDepositBalance() > 0) {
            notConfirmText.setText("");
            failReplenishment.setText("");
            startDepositButton.setVisible(true);
        }

        replenishButtonMain.setOnAction(event -> {
            if (!replenishFieldMain.getText().trim().matches(AccountController.floatRegex)) {
                failReplenishmentMain.setText("incorrect data format");
                replenishFieldMain.clear();
                return;
            }
            failReplenishment.setText("");
            failReplenishmentMain.setText("");
            notConfirmText.setText("");
            MainApplication.registeredCustomer.replenishMainAccount(Double.parseDouble(replenishFieldMain.getText()));
            mainAccMoney.setText(Double.toString(MainApplication.registeredCustomer.getMainBalance()));
            replenishFieldMain.clear();
            customerDataBaseHandler.updateCustomerInformation(MainApplication.registeredCustomer);

        });

        startDepositButton.setOnAction(event -> {
            failReplenishment.setText("");
            failReplenishmentMain.setText("");
            MainApplication.registeredCustomer.increaseCurrentMonth();
            currMonthText.setText(Integer.toString(0));
            EndOfDeposit.setText("Your deposit has been initiated");
            EndOfDeposit.setFill(Color.GREEN);
            startDepositButton.setVisible(false);
            customerDataBaseHandler.createCustomerTable(MainApplication.registeredCustomer);
            customerDataBaseHandler.updateCustomerInformation(MainApplication.registeredCustomer);
            customerDataBaseHandler.addInfoAboutCustomerMoney(MainApplication.registeredCustomer);
            drawChart(customerDataBaseHandler.fillArraysByMoneyAndMonths());
            skipOneMonth.setVisible(true);
        });

        withdrawalButton.setOnAction(event -> {
            failReplenishment.setText("");
            failReplenishmentMain.setText("");
            if (confirmCheckbox.isSelected()) {
                EndOfDeposit.setText("You terminated the deposit prematurely");
                EndOfDeposit.setFill(Color.GOLD);
                MainApplication.registeredCustomer.replenishMainAccount(MainApplication.registeredCustomer.getDepositBalance());
                endOfDepositButton.setVisible(true);
                skipOneMonth.setVisible(false);
                replenishButton.setDisable(true);
                replenishButtonMain.setDisable(true);
                replenishField.setDisable(true);
                replenishFieldMain.setDisable(true);
                withdrawalButton.setDisable(true);
                confirmCheckbox.setDisable(true);
                startDepositButton.setVisible(false);
                this.notConfirmText.setText("");
                customerDataBaseHandler.dropCustomerTable(MainApplication.registeredCustomer);
                MainApplication.registeredCustomer.setDefaultValues();
                depositMoneyText.setText(Double.toString(MainApplication.registeredCustomer.getDepositBalance()));
                mainAccMoney.setText(Double.toString(MainApplication.registeredCustomer.getMainBalance()));
                customerDataBaseHandler.addOfferToCustomer(MainApplication.registeredCustomer);
                customerDataBaseHandler.updateCustomerInformation(MainApplication.registeredCustomer);
            } else {
                notConfirmText.setText("You not confirm action");
            }
        });

        skipOneMonth.setOnAction(event -> {
            notConfirmText.setText("");
            failReplenishment.setText("");
            failReplenishmentMain.setText("");
            EndOfDeposit.setText("");
            MainApplication.registeredCustomer.increaseCurrentMonth();
            MainApplication.registeredCustomer.depositPayment();
            currMonthText.setText(Integer.toString(MainApplication.registeredCustomer.getCurrentMonth()));
            depositMoneyText.setText(Double.toString(MainApplication.registeredCustomer.getDepositBalance()));
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            customerDataBaseHandler.updateCustomerInformation(MainApplication.registeredCustomer);
            customerDataBaseHandler.addInfoAboutCustomerMoney(MainApplication.registeredCustomer);
            if (MainApplication.registeredCustomer.getCurrentMonth() >= 0) {
                drawChart(customerDataBaseHandler.fillArraysByMoneyAndMonths());
            }

            if (MainApplication.registeredCustomer.getCurrentMonth() >= MainApplication.registeredCustomer.getMonths()) {
                EndOfDeposit.setText("Your deposit has expired");
                MainApplication.registeredCustomer.replenishMainAccount(MainApplication.registeredCustomer.getDepositBalance());
                endOfDepositButton.setVisible(true);
                skipOneMonth.setVisible(false);
                replenishButton.setDisable(true);
                replenishButtonMain.setDisable(true);
                replenishField.setDisable(true);
                replenishFieldMain.setDisable(true);
                withdrawalButton.setDisable(true);
                confirmCheckbox.setDisable(true);
                customerDataBaseHandler.dropCustomerTable(MainApplication.registeredCustomer);
                MainApplication.registeredCustomer.setDefaultValues();
                depositMoneyText.setText(Double.toString(MainApplication.registeredCustomer.getDepositBalance()));
                mainAccMoney.setText(Double.toString(MainApplication.registeredCustomer.getMainBalance()));
                customerDataBaseHandler.addOfferToCustomer(MainApplication.registeredCustomer);
                customerDataBaseHandler.updateCustomerInformation(MainApplication.registeredCustomer);
            }

        });
        endOfDepositButton.setOnAction(event -> {
            endOfDepositButton.getScene().getWindow().hide();
        });
    }
}
