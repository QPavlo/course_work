package com.functional;

public class Deposit {
    private int depositID;
    private String bankName;
    private double percent;
    private int months;

    public Deposit() {

    }

    public Deposit(int depositID, String bankName, int months, double percent) {
        this.depositID = depositID;
        this.bankName = bankName;
        this.months = months;
        this.percent = percent;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public int getMonths() {
        return months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getDepositID() {
        return depositID;
    }

    public void setDepositID(int depositID) {
        this.depositID = depositID;
    }
}
