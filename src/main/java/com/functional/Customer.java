package com.functional;

public class Customer {
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private int customerID;
    private Deposit selectedDeposit;
    private double depositBalance;
    private double mainBalance;
    private int currentMonth;
    private int months;
    private int depositID;

    public Customer() {
        this.currentMonth = -1;
    }

    public Customer(String firstName, String lastName, String userName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        this.currentMonth = -1;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getDepositBalance() {
        return depositBalance;
    }

    public void setDepositBalance(double depositBalance) {
        this.depositBalance = depositBalance;
    }

    public double getMainBalance() {
        return Math.round(this.mainBalance * 100.0) / 100.0;
    }

    public void setMainBalance(double mainBalance) {
        this.mainBalance = mainBalance;
    }

    public int getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(int currentMonth) {
        this.currentMonth = currentMonth;
    }

    public int getMonths() {
        return months;
    }

    public int setMonths(int months) {
        if (this.selectedDeposit != null) {
            if (months >= this.selectedDeposit.getMonths() && (months % this.selectedDeposit.getMonths() != 0)) {
                if (months % this.selectedDeposit.getMonths() < this.selectedDeposit.getMonths() / 2) {
                    this.months = this.selectedDeposit.getMonths() * (months / this.selectedDeposit.getMonths());
                    return 1;
                } else {
                    this.months = this.selectedDeposit.getMonths() * ((months / this.selectedDeposit.getMonths()) + 1);
                    return 2;
                }
            } else if (months < this.selectedDeposit.getMonths()) {
                this.months = this.selectedDeposit.getMonths();
                return 3;
            } else {
                this.months = months;
                return 0;
            }
        } else {
            this.months = months;
            return 0;
        }
    }

    public int getDepositID() {
        return depositID;
    }

    public void setDepositID(int depositID) {
        this.depositID = depositID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Deposit getSelectedDeposit() {
        return selectedDeposit;
    }

    public void setSelectedDeposit(Deposit selectedDeposit) {
        this.selectedDeposit = selectedDeposit;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public void replenishDepositAccount(double amount) {
        this.depositBalance += amount;
    }

    public void increaseCurrentMonth() {
        this.currentMonth += 1;
    }

    public void replenishMainAccount(double amount) {
        this.mainBalance += amount;
    }

    public void withdrawFundsFromMainAccount(double amount) {
        if (amount <= this.mainBalance) {
            this.mainBalance -= amount;
        }
    }

    public void depositPayment() {
        if (this.currentMonth != 0 && (this.currentMonth % this.selectedDeposit.getMonths() == 0)) {
            this.depositBalance += (this.depositBalance * this.selectedDeposit.getPercent()) / 100;
            this.depositBalance = Math.round(this.depositBalance * 100.0) / 100.0;
        }
    }

    public void setDefaultValues() {
        this.depositBalance = 0;
        this.currentMonth = -1;
        this.months = 0;
        this.depositID = -1;
    }
}
