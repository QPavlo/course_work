package com.functional;

public class CustomerProgress {
    private int month;
    private double money;

    public CustomerProgress() {

    }

    public CustomerProgress(int month, double money) {
        this.month = month;
        this.money = money;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
}
