module com.example.courseworkap {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires java.sql;
    requires mysql.connector.java;

    opens com.functional to javafx.base;
    opens com.example.GUI to javafx.fxml;
    exports com.example.GUI;
}